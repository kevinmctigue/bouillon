module.exports = () => ({
  base: '/bouillon/',
  port: 4000,
  dest: 'public',
  head: [
    // ['link', { rel: 'icon', href: `/logo.png` }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    // ['link', { rel: 'apple-touch-icon', href: `/icons/apple-touch-icon-152x152.png` }],
    // ['link', { rel: 'mask-icon', href: '/icons/safari-pinned-tab.svg', color: '#3eaf7c' }],
    // ['meta', { name: 'msapplication-TileImage', content: '/icons/msapplication-icon-144x144.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
  ],
  themeConfig: {
    repo: 'https://gitlab.com/kevinmctigue/bouillon/tree/master/code',
    repoLabel: 'Code',
    editLinks: false,
    search: false,
    nav: require('./nav/en'),
    sidebar: getSidebar(),
  },
  plugins: [
    ['@vuepress/back-to-top', true],
    ['@vuepress/pwa', {
      serviceWorker: true,
      updatePopup: true
    }],
    ['@vuepress/medium-zoom', true],
    ['@vuepress/container', {
      type: 'vue',
      before: '<pre class="vue-container"><code>',
      after: '</code></pre>',
    }],
    ['@vuepress/container', {
      type: 'upgrade',
      before: info => `<UpgradePath title="${info}">`,
      after: '</UpgradePath>',
    }],
  ],
  extraWatchFiles: [
    '.vuepress/nav/en.js'
  ]
});

function getSidebar () {
  return [
    {
      title: '1. First Steps',
      children: [
        '',
        'tools',
        'haskell-setup',
      ].map(name => `introduction/${name}`)
    },
    {
      title: '2. The Front End',
      children: [
        '',
        'parser-combinators',
        'introducing-haskell',
        'a-haskell-parser',
      ].map(name => `front-end/${name}`)
    },
    {
      title: '3. Intermediate Form',
      children: [
        '',
        'composition',
      ].map(name => `intermediate-form/${name}`)
    },
    {
      title: '4. Webassembly',
      children: [
        '',
        'stack-machines',
      ].map(name => `webassembly/${name}`)
    }
  ]
}
