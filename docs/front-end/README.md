# Overview

The front end of the compiler is mostly concerned with parsing. Parsing is simply the process of stepping through a series of bits (usually text) and then grouping, structuring, annotating, and making sense of it all so that we can do more useful things (like compile it) in the back end.

In a typical compiler, the front end tends to have two important parts: the lexer and the parser. Often there are other pieces as well, but this is the simplest place to start:

![parser](./assets/front-end.svg)

### Tokens

When you look at a piece of text like this sentence your brain notices whole words--not individual letters--and you don't pay much attention to the spaces. The spaces are just there in order to see the words. A computer, on the other hand, can't just glance at a text and figure it out. All characters, including spaces, are created equal. They're just a string of bytes. So in order to interact with a program's source more like we do as humans we first need to break the text into **tokens**, sort of like our brain does. This allows us to write our parser in a much more intuitive way. In doing so we also discard all of the whitespace because we don't need it anymore.

### Lexer

The lexer is concerned with **lexical analysis**--the process of converting a stream of individual characters into tokens.

`input: text`

`output: tokens`

### Parser

Turns tokens into a **syntax tree** that can be compiled by the compiler's back end.

`input: tokens`

`output: syntax tree`

## Why Have a Lexer?

Lexers are usually defined by a **grammar**. There are a lot of really nice tools out there called **parser generators** that allow you to define your language syntax in terms of a grammar. They also usually take care of most of the parsing and turn your code into tokens for you. It's incredibly nice to one single document that defines the grammar of your entire language _and_ not having to write your own code that walks through text character-by-character and looks for patterns to group into tokens.

So if all that work has been done for us, you would have to be a crazy person to get rid of all this and write code that walks through text character-by-character yourself, right? Well, that's exactly what we're going to do.