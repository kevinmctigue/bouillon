# A Haskell Parser

The JS `parserCombinator` function on the last page was written to work as closely to Haskell's Parsec library as possible. For example, here's that `identParser` again in JS:

```js
const identParser = parserCombinator(parse =>
  parse(/^#t/).then(identStr =>
    Ident('true')
  )
);
```

And here it is in Haskell.

```haskell
identParser =
  (string "#t") >>= \identStr ->
    return True
```

We can already see how this is a bit simpler than the JS (and it will get even simpler). `string` is a function that comes from Haskell's `Parsec` library and it creates a parser combinator, just like `parserCombinator` from the above JS.

`\identStr -> ...` is the Haskell equivalent of an ES6 arrow function `(identStr) => ...`. The main difference you may see between the two is `>>=`. In our case that's the equivalent of `.then()`. Like `.then()`, it takes a wrapped value and lets you run a function on the value _inside_ the wrapper.

::: tip
`>>=` is known as the "bind operator". Whenever you read `>>=`, you can say "bind" in your head.
:::

But `>>=` is actually a lot more powerful than a Promise's `.then()`. This is because the data type that uses `>>=` is called a **monad** and it's much more generic than a Promise. In order to understand monads, let's talk about the problems with our current parser combinator library.

## Monads

Remember `exprParser`, our final parser that combined all the others? Well, we can combine that into even larger parsers. What if we wanted to parse two expressions in a row?

```
AND #t #t AND #t #t
```

We just need to take the output `ctx` from the first `exprParser`, look for a space, and plug the rest into another `exprParser`!

```js
const twoExprParser = ctx =>
  exprParser(ctx)
  .then(spaceParser)
  .then(exprParser);
```

Go ahead and add that to [parsec-usage.js](https://gitlab.com/kevinmctigue/bouillon/blob/master/code/chapter-1-front-end/parsec-usage.js) and try it for yourself.

But you may notice how `exprParser` and `twoExprParser` don't look the same as our other three parser functions. Because we didn't use `parserCombinator`, they have deal with that `ctx` object directly. That's because at some point we have to start the Promise chain by running our very first parser function with a starting `ctx`. That means that we still have to _think_ about that darn `ctx` sometime.

A second problem is that we don't yet have a parse tree. Let's take a closer look at the output of our `twoExprParser`:

```js
{
  input: 'AND #t #t AND #t #t',
  tokens:
   [ { type: 'Op', value: 'and' },
     { type: 'Ident', value: 'true' },
     { type: 'Ident', value: 'true' },
     { type: 'Op', value: 'and' },
     { type: 'Ident', value: 'true' },
     { type: 'Ident', value: 'true' } ],
  loc: 19
}
```

That's actually just a list of tokens. Now that's all fine so far because our code is flat too, but we want to be able to do things like this eventually:

```lisp
(AND #t
  (OR #t
    (OR #f #f)
  )
)
```

To parse something like that we need a tree, which means we need recursion. That's a hard thing to accomplish with the way our `parserCombinator` function works right now. We would have to concern our parser functions with the whole business of passing that `ctx` object around so that our `loc` can get incremented as needed.

So that's the tradeoff, huh? Either our parser functions have to deal with the `ctx` object or we can't do recursive parsers? Enter Haskell. Here's the Haskell equivalent of our `exprParser`.

```haskell
data Op
  = And

data Expr
  = Expr Op Bool Bool


identParser :: Parser Bool
identParser =
  (string "#t") >>= \identStr ->
    return True

opParser :: Parser Op
opParser =
  (string "AND") >>= \opStr ->
    return And

exprParser :: Parser Expr
exprParser =
  opParser
  >>= \op ->                         -- .then(op =>
    skipMany1 space                  --   skipMany1(space)
    >> identParser                   --   .then(identParser)
    >>= \identA ->                   --   .then(identA =>
      skipMany1 space                --     skipMany1(space)
      >> identParser                 --     .then(identParser)
      >>= \identB ->                 --     .then(identB =>
        return (Expr op identA identB) --       Op(op, identA, identB))))
```

First things first, there's this `data Op` and `data Expr`. That just creates a new data type, smilar to what we were doing with `const Op = ...` in the JS example. It's sort of like storing values in a box and sticking the label (like "Op" or "Expr") on it. More on this later.

We also have this new `>>` operator. It's almost the same as `>>=` except it discards the result of what's on its left. It's sort of like doing:

```js
.then(() => ...);
// The function takes no arguments, so it ignores whatever was in this .then
```

We do that in places like `skipMany1 space` because we don't care about the value that comes out of it. Just knowing the space is there is all we needed.

Now before we go any further, there's an easy way to clean up `exprParser` to make it a lot more readable:

```haskell
exprParser :: Parser Expr
exprParser = do
  op <- opParser
  skipMany1 space
  identA <- identParser
  skipMany1 space
  identB <- identParser
  return (Expr op identA identB)
```

_Much_ nicer! Notice that `do` in `exprParser = do`? This is what's called **do notation** and it allows us to keep our code flat when working with monads. It's similar to how `async/await` in JS allows us to keep our Promise code flat while maintaining all the same behavior. Just remember that this...

```haskell
op <- opParser
```
...is the same thing as

```haskell
opParser >>= \op ->
  ...
```

So now that you see how `>>=` and do notation work we can get back to talking about monads. Have you noticed that none of our Haskell parsers are passing around anything like a `ctx` object, including `exprParser`? That's because it's all handled behind-the-scenes. When you create a new data type and make it a monad you get to decide how `>>=` does its unwrapping. In the case of the Parsec library, they decided to give your parser functions the matched string, but _not_ to bother us with all the other details like the current position, etc.

_But_ once you run your function on that monad (like `>>= \op -> ...`), Parsec takes what our function returns and _re-wraps_ it in the same kind of monad as before. When it does that, the `ctx` gets updated for us! How does all that happen? The magic lies in `return`.

::: tip
Despite its name, `return` in Haskell it isn't at all like `return` statements in most other languages. We'll talk about that more later.
:::

Anyway, now we need to make our parser recursive, which doesn't take much more work at all. First we need parentheses in our language so that we can build a tree. We want to parse something like this:

```
(AND #t (AND #t #t))
```

The other change is to make expressions able to be composed of other expressions. So in `exprParser` we'll look for an operator `AND` followed by either an identifier _or_ another expression.

Here are the changes needed for that:

```haskell{5-6,19-22,26,29,31-33}
data Op
  = And

data Expr
  = ExprLeaf Bool
  | ExprBranch Op Expr Expr -- Now an Expr can be composed of other Exprs!


identParser :: Parser Bool
identParser =
  (string "#t") >>= \identStr ->
    return True

opParser :: Parser BoolOp
opParser =
  (string "AND") >>= \opStr ->
    return And

exprLeafParser :: Parser Expr
exprLeafParser =
  identParser >>= \ident ->
    return (ExprLeaf ident)

exprParser :: Parser Expr
exprParser = do
  char '('
  op <- opParser
  skipMany1 space
  exprA <- (exprLeafParser <|> exprParser)
  skipMany1 space
  exprB <- (exprLeafParser <|> exprParser)
  char ')'
  return (ExprBranch op exprA exprB)
```


Not too bad right, right? Now, before you cram your head with anymore new knowledge, let's take a moment to reap the fruits of our labor so far: Let's get this code running and see the result.

## Further Reading

[All About Monads](https://wiki.haskell.org/All_About_Monads)
