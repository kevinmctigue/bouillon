# Introducing Haskell

Let's take a minute to introduce the very basics of Haskell. I hope you'll find it as simple and elegant as I do.

To make a simple function that adds two numbers together, we could do it like this in JS:

```js
const add = (a, b) => a + b

add(1, 3);
```

Now let's see that in Haskell.

```haskell
-- Create the function...
add a b = a + b

-- Then you run it like so:
add 1 3
```

Simple, right? No parentheses or commas.

::: tip
"Running" a function is also called _applying_ the function, or "function application"
:::

Now, in our JS version `add` was defined as `(a, b) => a + b`, but to make it more like Haskell we would change it to...

```js
const add = a => b => a + b

// To apply this you would have to do
add(1)(3);
```

But in JavaScript you wouldn't usually do that because it becomes annoying to apply a function with all of those parentheses. There is a reason you would, though:

```js
const add = a => b => a + b
const add5 = add(5);

// `add(5)` will return function that expects one argument. You can prove that:
add5.toString(); // 'b => a + b'

// Apply it with 2
add5(2);
```

Just a second ago we learned that running a function is called **function application**...well what we just did above to make `add5` is known as **partial application** because we didn't give it all the arguments it needed.

What's nice about Haskell is every function can be partially applied without having to be specially defined to do so.

```haskell
add5 = add 5

-- Then you apply it like so:
add5 2
```

This becomes more apparent when you look at the **type signature** of a function. You'll see there's an arrow between _every_ argument, like in our second JS example `a => b => a + b`.

```haskell
add :: Int -> Int -> Int
add a b = a + b

add5 :: Int -> Int
add5 b = add 5 b

-- Creating add5 with partial application is the same thing, but shorter...
add5 :: Int -> Int
add5 = add 5
```

Notice how the type signature of both versions of `add5` are the same? That's because partially applying any function in Haskell returns a new function that expects the rest of the arguments.

Okay, that's all the syntax you need to know to start learning about parser combinators in Haskell. You'll pick up much more in the pages to come.