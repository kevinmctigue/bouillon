# More Haskell

Let's zoom in and see what each part is:

```haskell
               data Op = And
-- type constructor ^    ^ data constructor

-- Data constructors can have arguments too
   data Expr = Expr Op Bool Bool
--                  ^ arguments

-- You can also have multiple data constructors under the same type
   data Something
     = SomethingA Int
     | SomethingB String
     | SomethingC Bool
```

You can see in that second example, the type and constructor can actually have the same name. This is because the two are never used in the same place. The type is used in type signatures and the constructor is actually used in the functions.

```haskell
makeAndExpr :: Bool -> Bool -> Expr
makeAndExpr a b = --           ^ Expr type
  Expr And Bool Bool
-- ^ Expr constructor
```

exprLeafParser :: Parser Expr
exprLeafParser =
  ExprLeaf <$> identParser


<details>
<summary>Hey</summary>
Ok
</details>
