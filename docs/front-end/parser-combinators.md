# Parser Combinators

![parser combinator-inator](./assets/parser-combinator-inator.svg)

What you saw on the previous page is how most compiler front ends work. That's the traditional way to do it: Separate out the lexing from the parsing. That makes sense. Separation of the concerns is a great thing, right? In the functional programming world, though, there is a way to combine these two steps into one _without_ making our code messy. In fact, it will simplify things!

It's known as **parser combinators**. Parser combinators are functions that look for a specific pattern in a given piece of text, do their thang, and then return the rest of the text to be parsed by the next function. What's nice about parser combinator libraries is they do most of that work for us so we can just focus on finding the tokens in a given string.

Imagine we wanted to make a parser that just looked for `#t`. Let's pretend that we already have a library that gives us a `parserCombinator` function that's used like this:

```js
const { parserCombinator } = require('./parsec');

const identParser = parserCombinator(parse =>
  parse(/^#t/)
  .then(identStr => console.log(identStr))
);
```

1. `parserCombinator` gives us a `parse` function
1. We tell the `parse` function what kind of pattern we're looking for
1. It returns a Promise that has the matching string (if one was found)

Okay! We have our first parser function. Of course, it doesn't do anything useful with the result right now--it just logs the string. So let's wrap that matched string with a little bit more info and return it so that it can be combined with other parser functions:

```js{1,5}
const Ident = ident => ({ type: 'Ident', value: ident });

const identParser = parserCombinator(parse =>
  parse(/^#t/)
  .then(identStr => Ident('true'))
);
```

Lovely. Now, what if we wanted to also look for the Boolean operator `AND` so that the following text could be parsed:

```
AND #t #t
```

First we need the parser function for `AND`, which will be almost the same as the one for `#t`.

```js
const Op = op => ({ type: 'Op', value: op });

const opParser = parserCombinator(parse =>
  parse(/^AND/).then(opStr =>
    Op('and')
  )
);
```

We need to be able to handle spaces too. So let's do that. We're just going to be ignoring the result of this one.

```js
const spaceParser = parserCombinator(parse =>
  parse(/^\s+/).then(() => null)
);
```

## What We Have So Far

```js
const { parserCombinator } = require('./parsec');

const Ident = ident => ({ type: 'Ident', value: ident });
const Op = op => ({ type: 'Op', value: op });


const identParser = parserCombinator(parse =>
  parse(/^#t/).then(identStr =>
    Ident('true')
  )
);

const opParser = parserCombinator(parse =>
  parse(/^AND/).then(opStr =>
    Op('and')
  )
);

const spaceParser = parserCombinator(parse =>
  parse(/^\s+/).then(() => null)
);
```

Now we have all the parser functions we need...but how how do we combine them? That's the _combinator_ part of parser combinators. They're called parser combinators because they're functions that all share a standard input and output contract so that they can combine with each other to form more sophisticated parsers.

So to make a parser for the expression `AND #t #t` we just combine all the existing parser functions we have. But instead of using `parserCombinator` we'll make this one a plain function by hand.

```js
const exprParser = ctx =>
  opParser(ctx)
  .then(spaceParser)
  .then(identParser)
  .then(spaceParser)
  .then(identParser);
```

What is this `ctx`? It's just an object that contains the input string `AND #t #t`  as well as the current position of the parser. As we advance through the string we move the position forward so that we don't parse the same part of the string multiple times. For example, starting at `0`, the position will be `2` after it parses `AND`. The next parser will pick up with `#t #t`, and so on.

Well, it turns out that what `parserCombinator` was doing behind the scenes was wrapping our parser functions in another function to deal with `ctx`.

```js
const parserCombinator = parser => ctx => {
  // Take the string from ctx and run parser on it
  // Return a new ctx with the result of the parser
}
```

So when you run `const myParser = parserCombinator(...)` it returns another function that's just waiting for a `ctx` object. That's allows you to do `.then(myParser)` in the above Promise chain. This is the beauty of using a parser combinator library. We never had to even think about `ctx` when writing our parser functions, and we can chain them together and they will implicitly pass their `ctx` to one another.

Now, in order for this all to make sense we had to pretend that we already had a parser combinator library that gave us that `parserCombinator` function, but there is actually the working code for it [here](https://gitlab.com/kevinmctigue/bouillon-examples/chapter-1-front-end/parsec.js) so you can run the parsers we just wrote. If you were to run it (`node parsec-usage.js`) you should see this:

```js
{
  input: 'AND #t #t',
  tokens:
   [ { type: 'Op', value: 'and' },
     { type: 'Ident', value: 'true' },
     { type: 'Ident', value: 'true' } ],
  loc: 9
}
```

That's our final `ctx` object after everything is parsed. Just look at all those beautiful tokens! `loc` is that position mentioned earlier. It's now `9` because that's the end of the string.