# Running The Parser

If you haven't already, clone the [repo](https://gitlab.com/kevinmctigue/bouillon-examples) for this project.

```sh
$ git clone git@gitlab.com:kevinmctigue/bouillon-examples.git
$ cd bouillon
```

Then initialize Stack for this project:

```sh
$ cd code/chapter-2-front-end/haskell
$ stack init
```

You should now see a new file called `stack.yaml`.
