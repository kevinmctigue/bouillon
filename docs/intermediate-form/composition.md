# Function Composition

Let's go back to our `add` example: What if you wanted to chain together multiple functions? Let's say you wanted to add 2 to a number and then subtract that result from eight (yes, I know, a very contrived example...but stick with me). Given what we currently know about Haskell you could achieve that

```haskell
add2 :: Int -> Int
add2 n = 2 + n

subtractFrom8 :: Int -> Int
subtractFrom8 n = 8 - n

add2SubtractFrom8 :: Int -> Int
add2SubtractFrom8 n =
  subtractFrom8 $ add2 n
```

Now now that might be perfectly acceptable to most people...but in the Haskell world that would be considered _extremely_ verbose code. We can make it more succinct by using function composition.

When you partially apply a function, you create a new one that's just waiting for the rest of its arguments. Function composition allows you to take a bunch of partially applied functions and link them together so that the output of one becomes the input of the next.

So let's say we wanted add _another_ function to `add2SubtractFrom8`. After subtracting it would multiply by 5. We can do all that easily by using `.` to compose the 3 functions together.

```haskell
add2SubtFrom8Mult5 :: Int -> Int
add2SubtFrom8Mult5 =
  mult5 . subtractFrom8 . add2
```

Haskell uses `.` for composition because it's similar to the mathematical function composition symbol (`f ∘ g`) and it works pretty much the same way too!

Because `add2` needs one more argument, we create a function that's waiting for that one argument to continue. Once it gets that argument, it runs and then plugs its return value into next function (`subtractFrom8`), and `subtractFrom8` does the same to `mult5`.

This animation might help illustrate this idea:

![function composition](./assets/composition.gif)

It's very similar to using `$`:

```haskell
add2SubtFrom8Mult5 :: Int -> Int
add2SubtFrom8Mult5 n =
  mult5 $ subtractFrom8 $ add2 n
```

But notice that when using `$` we had to define `n` as an argument and then apply that to `add2`. That's because when you're using `$` you're writing your function from the perspective of when the function is being run, whereas with function composition you're creating a function that will behave in a certain way _when_ it has all the arguments it needs to run.

Function composition can make your code more succinct and it also allows you to create functions that can be passed around so that their arguments can be fulfilled at a later time. Here's another way we can shorten the code: Instead of creating separate functions like `add2`, Haskell allows us to compose operators too:

```haskell
add2SubtFrom8Mult5 :: Int -> Int
add2SubtFrom8Mult5 =
  (*) 5 . (-) 8 . (+) 2
```

This is where your mind really starts to bend. The reason you can do awesome stuff like partially applying an operator is because operators aren't something special like in most languages: Operators in Haskell are just functions--nothing more nothing less. They just happen to be named with a non-alphanumeric character instead of a word.

You might be thinking, "But...with an operator you put it between two values. How can it be a function?" That's true; that's what's called _infix notation_. But you can make your own infix operators in Haskell by defining a function that takes exactly two arguments and then [telling Haskell](https://github.com/ghc/ghc/blob/master/libraries/base/GHC/Num.hs#L29) it's an infix function...but we won't get into that now.

::: tip
You can actually run any function with infix notation without defining it as one! All it takes is putting the function inside some backticks.

```haskell
combine :: String -> String -> String
combine a b =
  a ++ ", " ++ b

-- I guess you _could_ run it like any old function...
combine "Hello" "world"

-- But it's so much cooler to be an infix function
"Hello" `combine` "world"

-- Which allows you to do things like this:
"Hello" `combine` "world" `combine` "solar system" `combine` "galaxy"
```
:::

#### Anyway, back to function composition.

Haskell also has a way to compose functions from left to right (`>>>`) so that it reads more like English or piping in UNIX. It seems that this isn't super popular in the Haskell world, where most composition is done using `.`, but I think it can help solidify the idea of function composition and, hey, if it works better for you then use it!

```haskell
import Control.Category ((>>>))

add2SubtFrom8Mult5 :: Int -> Int
add2SubtFrom8Mult5 =
  (+) 2 >>> (-) 8 >>> (*) 5
```

![function composition](./assets/composition-lr.gif)

Now, scroll back up to the top and look at how much more elegant this is than where we started.
