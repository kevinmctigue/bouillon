# Introduction

You've probably heard about WebAssembly a few times in the last year, and you probably get the main idea: Browsers being able to run code that's not JavaScript. How does that actually work though? Well, the best way to answer that is by digging in and building something.

What we'll do here is build a super simple language that can evaluate Boolean expressions. To make that happen we will build a little compiler that takes in our language and outputs WebAssembly. The language will look like this:

```lisp
(AND #t (OR #t #f))
```

That's it! Only four identifiers in this language: `AND`, `OR`, `#t`, and `#f`. We will be compiling that into WebAssembly's human-readable text format first. The above code will compile into the following:

```lisp
(module
  (func (export "main") (result i32)
    i32.const 1
    i32.const 1
    i32.const 0
    i32.or
    i32.and
))
```

Which then gets turned into WebAssembly _binary_ format:

```
0000000 00 61 73 6d 01 00 00 00 01 05 01 60 00 01 7f 03 | .asm.......`....
0000010 02 01 00 07 08 01 04 6d 61 69 6e 00 00 0a 0c 01 | .......main.....
0000020 0a 00 41 01 41 01 41 00 72 71 0b                | ..A.A.A.rq.
000002b
```

### Who This Guide Is For

This guide was written with a specific kind of developer in mind: Web developers who want to expand their knowledge beyond JavaScript and maybe beyond web development entirely. Of course, this guide is not _only_ for web developers. If you're interested in WebAssembly, parser combinators, or making a language you'll probably find a lot of value here too.

With web developers in mind, the only language you are expected to know coming into this is JavaScript. The first few chapters will use JS syntax to explain certain concepts and then we will slowly transition to Haskell (more on that choice later). So besides WebAssembly, by the time you've worked your way through this guide you will also have picked up several other powerful concepts:

- Basic lisp syntax
- Functional programming with Haskell
- The basics of language parsing
- Parser combinators
- Stack machines

...and some other useful stuff too.

