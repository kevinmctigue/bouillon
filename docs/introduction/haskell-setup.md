# Haskell Setup

Haskell's main package manager is called **Cabal**, but there's another (arguably simpler) package manager called **Stack**. You can think of them like this:

- Cabal = NPM
- Stack = Yarn

We'll be using Stack here because it's dead simple to get going.

Instructions for most platforms are [here](https://docs.haskellstack.org/en/stable/README/#how-to-install). If you're on a Mac, just run:

```sh
brew install haskell-stack
```

```sh
stack run "(AND #t (AND #t #t))"
```
