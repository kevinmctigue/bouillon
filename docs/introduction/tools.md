# Tools And Docs

## The Docs

These are just here because I refer to them at times and they're worth sharing. Don't feel obligated to read them cover-to-cover.

- [Mozilla WebAssembly Docs](https://developer.mozilla.org/en-US/docs/WebAssembly)
- [Write Yourself a Scheme in 48 Hours [PDF]](https://upload.wikimedia.org/wikipedia/commons/a/aa/Write_Yourself_a_Scheme_in_48_Hours.pdf)
- [WebAssembly Illustrated](https://takenobu-hs.github.io/downloads/WebAssembly_illustrated.pdf)
- [WebAssembly Reference Manual](https://github.com/sunfishcode/wasm-reference-manual)
- [WebAssembly Docs](https://webassembly.org)

## The Tools

Here are the main tools we'll be using for this guide. Webassembly Studio is web-based and we'll set up Haskell later, so you don't have to get these now.

- [Webassembly Studio](https://webassembly.studio)
- [Haskell](haskell.org)
- [Haskell Stack](https://docs.haskellstack.org/en/stable/README)
