# WebAssembly

There are several WebAssembly guides out there, but most of them seem to fall into two camps:

1. High-level "Here's how to compile _[language X]_ to WebAssembly" guides that never get into how WebAssembly actually works
1. Guides that assume large amounts of prior knowledge about lower-level language implementation (things like memory management, stack machines, etc.)

We will be diving deep into the workings of WebAssembly without assuming prior knowledge on low level languages. The tradeoff is that it can't be fit into a single Medium post (and I consider that a good thing).

### WebAssembly isn't Assembly

"Assembly" almost always means human-readable code that compiles (almost) directly down to machine instructions. WebAssembly is not that. WebAssembly actually runs on a VM. This VM is a stack machine, which means it has a stack of instructions and executes them in the order they are pushed to it (we'll get much deeper into this in the next chapter).

#### "Then why do I hear about better performance than JavaScript?"

 Well for one, WebAssembly instructions are super low-level compared to the kind of languge any (sane) person would write an app in, so those instructions can be mapped pretty well to real machine instructions for most CPUs. But besides that, pretty much all of the performance gains you hear about just come from using high-performance languages like Rust or C which don't have to dynamically allocate or garbage collect memory. They are also statically typed which opens the door up to many more optimizations that just aren't possible with JavaScript.

### Why WebAssembly Matters

Usually when we hear about WebAssembly, there are two big talking points:

1. **Language independence**
1. **Performance**

Imagine being able to code for the web in any language you wanted--no more JavaScript, no more `Cannot read property 'x' of undefined`. Better yet, in situations where performance matters you can use a language like C or Rust that far outperform JavaScript.

While those are both fantastic gains for the web, it's hard underestimate the impact WebAssembly will have on computing as a whole. To explain that bold statement, let's take a quick trip back through computer history.

### Why WebAssembly _Really_ Matters

There was a time when if you wanted the wonderful compile-time checks and speed that came with a compiled language, you had to build your application for every platform that mattered to you--that meant compiling a different binary for every CPU architecture/OS combination. Then came Java. Suddenly a developer could compile their code once and run it everywhere. What's more, the consumer of that application could feel fairly safe about that code running on their machine because it was sandboxed in a VM. These were huge wins in their day.

Of course, Java has its drawbacks. The main one being that it's owned and [controlled](https://en.wikipedia.org/wiki/Oracle_America,_Inc._v._Google,_Inc.) by a single company. But what if your browser had a VM that ran a similar kind of bytecode VM. Now you have all the benefits that come with having a VM (cross-platform compiles, memory safety, sandboxing), but this one is special--it's also the means of distributing that code to the world! And of course the kicker is that, unlike the JVM, it's *completely open*! No single company controls it.

This is the real reason why WebAssembly is important. It releases both the developers and the consumers of software from having to run any specific platform _and_ it sets us free from the control any single _company_ too!
