
# The Stack Machine


### Why WebAssembly Is Secure

Letting a program have direct access to your memory is a big no-no. One way to allocate memory safely is by letting it _pretend_ that it's allocating memory itself, but really it's asking another program (in our case the WebAssembly VM) to do the allocation on our program's behalf.

::: tip
In most cases that program then asks the OS to allocate the memory on _its_ behalf...I know, lots of passing the buck.
:::

There are tons of different ways to organize this process of safely allocating memory while still giving the program the ability to access that data. The good folks who thought up WebAssembly decided on a **stack machine** as their VM of choice. As you'll see, it's actually a pretty great choice. It's easy to keep it in your head and imagine what's going on.

The central concept of a stack machine is that it uses a single stack to manage its memory. All of the arguments to a function or stacked on top of one another and then popped back off the stack when the function is called. Then the return value of that function gets put back at of the stack. It's a very simple way to keep both your memory and instructions organized: When things always happen sequentially, you can think through your code step-by-step to understand it.

Let's take a look at our favorite contrived example: Adding and subtracting numbers!

```
i32.const 4
i32.const 2
i32.add
i32.const 1
i32.sub
```

This can be read as:

1. Start with an empty stack
1. Push `4` to the stack
1. Push `2` on top `4`
1. Pop the top 2 items from the stack, add them, and put the result back on the top of the stack
1. Push `1` to the stack
1. Subtract `1` from `6`

![simple stack machine](./assets/stack-simple.gif)

The subtraction is the only one that behaves a little strange on first glance: The `1` gets popped off the stack first, followed by `5`...but it ends up getting evaluated as `5 - 1`. Why is that?

**The order arguments are applied is by the order they are _pushed to_ the stack not _popped from_ it**.

So to just do `5 - 1` in WebAssembly would be:

```
i32.const 5
i32.const 1
i32.sub
```
